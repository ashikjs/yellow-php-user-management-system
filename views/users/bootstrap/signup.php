<?php
include_once "../../../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" href="http://www.themeyellow.com/assets/images/fav.png">

    <title>Yellow - PHP User Management System</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.css" rel="stylesheet">
    <link href="build/css/form.style.css" rel="stylesheet">
  </head>

  <body class="signup">
      <div class="form_wrapper">
        <div id="register" class="animate form registration_form">
          <section class="form_content">
            <form action="signup-process.php" method="post" id="signupForm">
                <h1>Create Account</h1>
              <strong class="suc"><?php $obj->ValidationMessage("storeSuccess"); ?></strong>
              <div>
                <input type="text" name="user_name" class="form-control" placeholder="Username"/>
              </div>
              <div>
                <input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
              </div>
              <div>
                <input type="password" name="re_password" class="form-control" placeholder="Repassword"/>
              </div>
              <div>
                <input type="email" name="email" class="form-control" placeholder="Email"/>
              </div>
                  <div>
                      <div class="g-recaptcha" data-sitekey="6LfkNSkTAAAAAC-KL5R4H__lxNVzPZpz2NGcEyZB"></div>
                      <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                  </div>
                      <div>
                          <button class="btn btn-default submit">Sign Up</button>
                      </div>

                      <div class="clearfix"></div>
                      <div class="separator">
                          <p class="change_link">Already have an account?
                              <a href="login.php" class="to_register"> Log in </a>
                          </p>

                          <div class="clearfix"></div>
                          <br />
                          <div>
                              <h1><i class="fa fa-shield"></i> Yellow</h1>
                              <p>©2016 All Rights Reserved.</p>
                          </div>
                      </div>
            </form>
          </section>
        </div>
      </div>

      <!-- jQuery -->
      <script src="vendors/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap -->
      <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- FastClick -->
      <script src="vendors/fastclick/lib/fastclick.js"></script>
      <!-- NProgress -->
      <script src="vendors/nprogress/nprogress.js"></script>
      <!-- iCheck -->
      <script src="vendors/iCheck/icheck.min.js"></script>
      <!-- recaptcha api -->
      <script src='https://www.google.com/recaptcha/api.js'></script>
      <!-- jquery.validate -->
      <script src="vendors/jqueryvalidation/jquery.validate.min.js"></script>
      <script src="vendors/jqueryvalidation/additional-methods.min.js"></script>

      <!-- custom validation script -->
      <script>
          /*User Name*/
          $.validator.addMethod("user_name_valid", function (value) {
              if (/^[a-zA-Z0-9_\.]+$/.test(value)) {
                  return true;
              } else {
                  return false;
              }
          }, "The username can only consist of alphabetical, number, dot and underscore");

          /*Password*/
          $.validator.addMethod("pwcheck", function (value) {
              return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) && /[a-z]/.test(value) && /\d/.test(value)
          });

          /*Email*/
          $.validator.addMethod("validemail", function (value) {
              if (value == '')
                  return true;
              var temp1;
              temp1 = true;
              var ind = value.indexOf('@');
              var str2 = value.substr(ind + 1);
              var str3 = str2.substr(0, str2.indexOf('.'));
              if (str3.lastIndexOf('-') == (str3.length - 1) || (str3.indexOf('-') != str3.lastIndexOf('-')))
                  return false;
              var str1 = value.substr(0, ind);
              if ((str1.lastIndexOf('_') == (str1.length - 1)) || (str1.lastIndexOf('.') == (str1.length - 1)) || (str1.lastIndexOf('-') == (str1.length - 1)))
                  return false;
              str = /(^[a-zA-Z0-9]+[\._-]{0,1})+([a-zA-Z0-9]+[_]{0,1})*@([a-zA-Z0-9]+[-]{0,1})+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,15})$/;
              temp1 = str.test(value);
              return temp1;
          }, "Please enter valid email");

          $("#signupForm").validate({
              ignore: ".ignore",
              rules: {
                  user_name: {
                      user_name_valid: true,
                      required: true,
                      minlength: 6,
                      maxlength: 12,
                      remote: "validations.php?data=name"
                  },
                  password: {
                      pwcheck: true,
                      required: true,
                      minlength: 6,
                      maxlength: 12
                  },
                  re_password: {
                      required: true,
                      equalTo: "#password"
                  },
                  email: {
                      validemail: true,
                      required: true,
                      email: true,
                      remote: "validations.php?data=email"
                  },
                  hiddenRecaptcha: {
                      required: function () {
                          if (grecaptcha.getResponse() == '') {
                              return true;
                          } else {
                              return false;
                          }
                      }
                  }
              },
              messages: {
                  hiddenRecaptcha: "You must complete the anti spam verification",
                  user_name: {
                      required: "Please enter a username",
                      minlength: "Please enter 6-12 characters",
                      maxlength: "Please enter 6-12 characters",
                      remote: $.validator.format("'{0}' is already exists")
                  },
                  password: {
                      pwcheck: "Allowed Characters: 'A-Z a-z 0-9 @ * _ - . !' for password",
                      required: "Please provide a password",
                      minlength: "Please enter 6-12 characters",
                      maxlength: "Please enter 6-12 characters"
                  },
                  re_password: {
                      required: "Please provide a re-password",
                      equalTo: "Please enter the same password as above"
                  },
                  email: {
                      required: "Please provide a email address",
                      email: "Please enter a valid email address",
                      remote: $.validator.format("'{0}' is already exists")
                  }
              }
          });
      </script>
      <!-- Custom Theme Scripts -->
      <script src="build/js/custom.js"></script>
  </body>
</html>
