<?php
include_once "../../../vendor/autoload.php";

use App\Users\Users;

$obj = new Users;
$obj->prepare($_POST);
$forgotdata = $obj->forgot();
$id = $_POST['id'];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // password
    if (!empty($_POST['email']) && $_POST['id'] == "reset") {
        $to = $_POST['email'];
        $subject = 'Forgot | Verification';
        $message = '
       Hello,<br><br>
       A request has been made to reset your account password. <br><br>To reset your password, you will need to submit this verification code in order to verify that the request was legitimate.<br><br>
        Select the URL below and proceed with resetting your password.<br><br>
        http://themeyellow.com/yellow/views/users/materialize/forgot-verify.php?vid=' . $forgotdata['verification_id'] . '<br><br>
        Thank you.';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From:ThemeYellow<info@themeyellow.com>' . "\r\n";
        $mail = @mail($to, $subject, $message, $headers);
        if (isset($mail) && !empty($mail)) {
            $_SESSION['foM'] = "Successfully sent. Please check your mail.";
            header("location:forgot.php?id=$id");
        } else {
            $_SESSION['Nots'] = "Something going wrong. we can't sent you email :(";
            header("location:forgot.php?id=$id");
        }
    }
    if ($_POST['id'] == "pconfirm") {
        $obj->forgotPasswordQuery();
    }
    if ($_POST['id'] == "pcomplete") {
        $obj->forgotPasswordUpdate();
    }
    // end password

    // user name email
    if (!empty($_POST['email']) && $_POST['id'] == "remind") {
        $to = $_POST['email'];
        $subject = 'Your Username';
        $message = '
        Hello,<br><br>
        A username reminder has been requested for your ThemeYellow account.<br><br>
        Your username is ' . $forgotdata['user_name'] . '.<br><br>
        To login to your account, select the link below.<br><br>
        http://themeyellow.com/yellow/<br><br>
        Thank you.
        ';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From:ThemeYellow<info@themeyellow.com>' . "\r\n";
        $mail = @mail($to, $subject, $message, $headers);
        if (isset($mail) && !empty($mail)) {
            $_SESSION['foM'] = "Successfully sent. Please check your mail.";
            header("location:forgot.php?id=$id");
        } else {
            $_SESSION['Nots'] = "Something going wrong. we can't sent you email :(";
            header("location:forgot.php?id=$id");
        }
    }

    //resent
    if (!empty($_POST['email']) && $_POST['id'] == "resent") {
        if ($forgotdata['is_active'] == 0) {
            $to = $_POST['email'];
            $subject = 'SignUp | Verification';
            $message = '
            Thanks for signing up!<br><br>
            Activated your account by pressing the url below.<br><br>
            ------------------------<br>
            Username: ' . $forgotdata['user_name'] . '<br><br>
            Password: ' . $forgotdata['password'] . '<br>
            ------------------------<br><br>
            Please click this link to activate your account:<br><br>
            http://themeyellow.com/yellow/views/users/materialize/verify.php?vid=' . $forgotdata['verification_id'] . '<br><br>
            Thank you.
            ';

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From:ThemeYellow<info@themeyellow.com>' . "\r\n";
            $mail = @mail($to, $subject, $message, $headers);
            if (isset($mail) && !empty($mail)) {
                $_SESSION['foM'] = "Successfully sent. Please check your mail.";
                header("location:forgot.php?id=$id");
            } else {
                $_SESSION['Nots'] = "Something going wrong. we can't sent you email :(";
                header("location:forgot.php?id=$id");
            }
        } else {
            $_SESSION['Nots'] = "Email Already Verified.";
            header("location:forgot.php?id=$id");
        }
    }

} else {
    $_SESSION['Errors_R'] = "404 not found :(";
    header("location:errors.php");
}
?>